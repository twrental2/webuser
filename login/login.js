import { getToken } from "./getToken.js";
import { checkLogged } from "../carRent.js";

const Uname = $("#Uname");
const passWord = $("#passWord");
const submitButton = $("#submitButton");
const greeting = $(".greeting");
const nv_cars_btn = $("#nv-cars-btn");
const nv_login_btn = $("#nv-login-btn");
const nv_bookings_btn = $("#nv-bookings-btn");
const nv_logout_btn = $("#nv-logout-btn");
const view = $("#view");

submitButton.click(async () => {
  await getToken(Uname.val(), passWord.val()).then((res) => {
    console.log(checkLogged());
    if (checkLogged()) {
      view.empty();
      greeting.removeClass("hide");
      greeting.text("welcome " + Uname.val());
      
      nv_cars_btn.removeClass("hide");
      nv_login_btn.addClass("hide");
      nv_bookings_btn.removeClass("hide");
      nv_logout_btn.removeClass("hide");
    } else {
      alert("incorrect username or password");
    }
  });
});
