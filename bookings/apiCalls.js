import { checkExp } from "../login/getToken.js";

export const listCustomerOrders = async () => {
  await checkExp().then((res) => {
    if (res === true) accecc_token = localStorage.getItem("anasToken");
  });
  const output = {
    success: false,
    bookings: [],
  };
  await fetch("http://localhost:9090/api/v1/myorders", {
    method: "GET",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("anasToken"),
      accept: "*/*",
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": "*",
      name: localStorage.getItem("userName"),
    },
  })
    .then(async (res) => {
      if (res.status === 200) {
        const parsed = await res.json();
        output.success = true;
        output.bookings = parsed;
      }
    })
    .catch(() => {
      console.log("an error accured");
    });

  return output;
};



export const updateBooking = async (bookingId) => {
  let result;

  await fetch("http://localhost:9090/api/v1/updateorder", {
    method: "PUT",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("anasToken"),
      accept: "*/*",
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": "*",
      contractId: bookingId,
    },
  })
    .then(async (res) => {
      if (res.status === 200) {
        // output.success = true;
        result = await res.json();
      }
    })
    .catch(() => {
      console.log("an error accured");
    });

  return result;
};
