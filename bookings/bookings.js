import { listCustomerOrders } from "../bookings/apiCalls.js";
import { updateBooking } from "./apiCalls.js";

const user_name = localStorage.getItem("userName");
const orders_table = $("#ordersTablee");
let orders = [];
const rows = $("td");

export const getOrders = async () => {
  orders_table.removeClass("hidden");
  rows.empty();
  await listCustomerOrders().then((res) => {
    console.log(res);
    res.bookings.forEach((booking) => {
      const order_row = $("<tr></tr>");
      const td_id = $(`<td>${booking.id}</td>`);
      const td_car = $(`<td>${booking.car.brand}</td>`);
      let td_booking_date = $(`<td>${booking.createAt}</td>`);
      const td_booking_update = $(`<td><button>update</button></td>`);
      order_row.append(td_id, td_car, td_booking_date, td_booking_update);
      orders_table.append(order_row);

      td_booking_update.click(async () => {
        await updateBooking(booking.id).then((res) => {
          console.log(res.createAt);
          td_booking_date.remove();
          td_booking_update.remove();
          td_booking_date = res.createAt;
          order_row.append(td_booking_date, td_booking_update);
        });
      });
    });
  });
};
