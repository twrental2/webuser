import { clearSession } from "./login/logout.js";
import { getOrders } from "./bookings/bookings.js";

$(".greeting").addClass("hide");

const nv_cars_btn = $("#nv-cars-btn");
const nv_login_btn = $("#nv-login-btn");
const nv_bookings_btn = $("#nv-bookings-btn");
const nv_logout_btn = $("#nv-logout-btn");
const view = $("#view");
const greeting = $(".greeting");

export const checkLogged = () => {
  const token = localStorage.getItem("anasToken");
  const storedUserName = localStorage.getItem("userName");
  if (token !== null && storedUserName !== null) {
    return true;
  } else {
    // view.load("/index.html .companyName");
    nv_cars_btn.addClass("hide");
    nv_bookings_btn.addClass("hide");
    nv_logout_btn.addClass("hide");
    nv_login_btn.removeClass("hide");
    greeting.addClass("hide");
    return false;
  }
};

checkLogged();

export const showCars = () => {
  view.load("/cars/carList.html");
  $("#ordersTablee").addClass("hidden");
};

nv_bookings_btn.click(async () => {
  // view.html = "";
  await getOrders();
  view.load("/bookings/bookings.html");
  $("#ordersTablee").removeClass("hidden");
});

export const showLogin = () => {
  view.load("/login/login.html");
  $("#ordersTablee").addClass("hidden");
};

// when click the login we will load login page to the main page
// and remove company name
nv_login_btn.click(function () {
  $(".companyName").addClass("hide");
  // view.load("/login/login.html");
  showLogin();
});

// when click the cars, we will load carsList page to the main page
// and these cars are available cars in db
nv_cars_btn.click(function () {
  // view.load("/cars/carList.html");
  showCars();
  greeting.addClass("hide");
});

// onclick logout website will ask user to confirm the logout process
nv_logout_btn.click(async () => {
  $("#ordersTablee").addClass("hidden");
  if (confirm("are you sure to logout:")) {
    await clearSession(); // session in keycloak
    localStorage.clear();
    //checkLogged()
    //view.html('')
    view.empty();
    // view.load("/login/login.html");
    view.load("/index.html .companyName");
    nv_cars_btn.addClass("hide");
    nv_bookings_btn.addClass("hide");
    nv_logout_btn.addClass("hide");
    nv_login_btn.removeClass("hide");
    greeting.addClass("hide");
  } else {
    checkLogged();
  }
});
